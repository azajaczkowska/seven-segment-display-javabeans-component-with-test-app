import java.awt.Color;

public class ApplicationTest extends javax.swing.JFrame {

    private String displayNumber;
    private String[] numberArrayString;
    private int counter = 0;
    private boolean isNumeric;

    public ApplicationTest() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator4 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jComboBox2 = new javax.swing.JComboBox<>();
        sevenSegment1 = new SevenSegmentComponent.SevenSegment();
        sevenSegment2 = new SevenSegmentComponent.SevenSegment();
        sevenSegment3 = new SevenSegmentComponent.SevenSegment();
        sevenSegment4 = new SevenSegmentComponent.SevenSegment();
        sevenSegment5 = new SevenSegmentComponent.SevenSegment();

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel2.setText("Wpisz liczby do wyświetlenia ");

        jButton1.setText("Wyświetl");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Kolor wyświetlacza:");

        jLabel3.setText("Ramka:");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Czerwony", "Zielony", "Niebieski" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Brak", "Wklęsła" }));
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
                        .addGap(80, 80, 80)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(sevenSegment1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22)
                        .addComponent(sevenSegment2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(sevenSegment3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23)
                        .addComponent(sevenSegment4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sevenSegment5, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(sevenSegment2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sevenSegment1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sevenSegment3, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sevenSegment4, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sevenSegment5, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(11, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       displayNumbers();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        String selectedItem = new String();
        selectedItem = (String) jComboBox1.getSelectedItem();
        switch(selectedItem) {
            case "Czerwony":
                sevenSegment1.setColor(Color.RED);
                sevenSegment2.setColor(Color.RED);
                sevenSegment3.setColor(Color.RED);
                sevenSegment4.setColor(Color.RED);
                sevenSegment5.setColor(Color.RED);
                break;
            case "Zielony":
                sevenSegment1.setColor(Color.GREEN);
                sevenSegment2.setColor(Color.GREEN);
                sevenSegment3.setColor(Color.GREEN);
                sevenSegment4.setColor(Color.GREEN);
                sevenSegment5.setColor(Color.GREEN);
                break;
            case "Niebieski":
                sevenSegment1.setColor(Color.BLUE);
                sevenSegment2.setColor(Color.BLUE);
                sevenSegment3.setColor(Color.BLUE);
                sevenSegment4.setColor(Color.BLUE);
                sevenSegment5.setColor(Color.BLUE);
                break;
        }
        repaint();
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ApplicationTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ApplicationTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ApplicationTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ApplicationTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ApplicationTest().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JTextField jTextField1;
    private SevenSegmentComponent.SevenSegment sevenSegment1;
    private SevenSegmentComponent.SevenSegment sevenSegment2;
    private SevenSegmentComponent.SevenSegment sevenSegment3;
    private SevenSegmentComponent.SevenSegment sevenSegment4;
    private SevenSegmentComponent.SevenSegment sevenSegment5;
    // End of variables declaration//GEN-END:variables

    private void displayNumbers() {
        
        counter = 0;
        displayNumber = jTextField1.getText();
        numberArrayString = new String[displayNumber.length()];
        
        for (int i = 0; i < displayNumber.length(); i++) {
            numberArrayString[i] = displayNumber.substring(counter, counter + 1);
            counter++;
        }

        try {
            double d = Double.parseDouble(displayNumber);
            isNumeric = true;
        } catch (NumberFormatException nfe) {
            isNumeric = false;
        }

        if (isNumeric && displayNumber.length() == 5) {
            sevenSegment1.setNumber(numberArrayString[0]);
            sevenSegment2.setNumber(numberArrayString[1]);
            sevenSegment3.setNumber(numberArrayString[2]);
            sevenSegment4.setNumber(numberArrayString[3]);
            sevenSegment5.setNumber(numberArrayString[4]);
        }
        if (isNumeric && displayNumber.length() == 4) {
            sevenSegment1.setNumber("null");
            sevenSegment2.setNumber(numberArrayString[0]);
            sevenSegment3.setNumber(numberArrayString[1]);
            sevenSegment4.setNumber(numberArrayString[2]);
            sevenSegment5.setNumber(numberArrayString[3]);
        }
        if (isNumeric && displayNumber.length() == 3) {
            sevenSegment1.setNumber("null");
            sevenSegment2.setNumber("null");
            sevenSegment3.setNumber(numberArrayString[0]);
            sevenSegment4.setNumber(numberArrayString[1]);
            sevenSegment5.setNumber(numberArrayString[2]);
        }
        if (isNumeric && displayNumber.length() == 2) {
            sevenSegment1.setNumber("null");
            sevenSegment2.setNumber("null");
            sevenSegment3.setNumber("null");
            sevenSegment4.setNumber(numberArrayString[0]);
            sevenSegment5.setNumber(numberArrayString[1]);
        }
        if (isNumeric && displayNumber.length() == 1) {
            sevenSegment1.setNumber("null");
            sevenSegment2.setNumber("null");
            sevenSegment3.setNumber("null");
            sevenSegment4.setNumber("null");
            sevenSegment5.setNumber(numberArrayString[0]);
        }
        if (isNumeric && displayNumber.length() == 0) {
            sevenSegment1.setNumber("null");
            sevenSegment2.setNumber("null");
            sevenSegment3.setNumber("null");
            sevenSegment4.setNumber("null");
            sevenSegment5.setNumber("null");
        }
        if (!isNumeric) {
            sevenSegment1.setNumber("e");
            sevenSegment2.setNumber("r");
            sevenSegment3.setNumber("r");
            sevenSegment4.setNumber("o");
            sevenSegment5.setNumber("r");
        }

        repaint();
    }
}
