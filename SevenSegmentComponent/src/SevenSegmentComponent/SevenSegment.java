package SevenSegmentComponent;

import java.awt.*;

public class SevenSegment extends Component {

    private Color activeColor = new Color(255, 0, 0);
    private Color inactiveColor = new Color(211, 211, 211);
    private Dimension size;
    int segment[] = new int[7];

    public SevenSegment() {
        size = new Dimension(100, 200);
        this.setMinimumSize(size);
    }

    public Color getColor() {
        return activeColor;
    }

    public void setColor(Color c) {
        activeColor = c;
    }

    private Boolean isNumeric;

    public void setNumber(String val) {

        try {
            double d = Double.parseDouble(val);
            isNumeric = true;
        } catch (NumberFormatException nfe) {
            isNumeric = false;
        }

        if (isNumeric) {
            switch (val) {
                case "1":
                    segment[0] = 0;
                    segment[1] = 1;
                    segment[2] = 1;
                    segment[3] = 0;
                    segment[4] = 0;
                    segment[5] = 0;
                    segment[6] = 0;
                    break;
                case "2":
                    segment[0] = 1;
                    segment[1] = 1;
                    segment[2] = 0;
                    segment[3] = 1;
                    segment[4] = 1;
                    segment[5] = 0;
                    segment[6] = 1;
                    break;
                case "3":
                    segment[0] = 1;
                    segment[1] = 1;
                    segment[2] = 1;
                    segment[3] = 1;
                    segment[4] = 0;
                    segment[5] = 0;
                    segment[6] = 1;
                    break;
                case "4":
                    segment[0] = 0;
                    segment[1] = 1;
                    segment[2] = 1;
                    segment[3] = 0;
                    segment[4] = 0;
                    segment[5] = 1;
                    segment[6] = 1;
                    break;
                case "5":
                    segment[0] = 1;
                    segment[1] = 0;
                    segment[2] = 1;
                    segment[3] = 1;
                    segment[4] = 0;
                    segment[5] = 1;
                    segment[6] = 1;
                    break;
                case "6":
                    segment[0] = 1;
                    segment[1] = 0;
                    segment[2] = 1;
                    segment[3] = 1;
                    segment[4] = 1;
                    segment[5] = 1;
                    segment[6] = 1;
                    break;
                case "7":
                    segment[0] = 1;
                    segment[1] = 1;
                    segment[2] = 1;
                    segment[3] = 0;
                    segment[4] = 0;
                    segment[5] = 0;
                    segment[6] = 1;
                    break;
                case "8":
                    segment[0] = 1;
                    segment[1] = 1;
                    segment[2] = 1;
                    segment[3] = 1;
                    segment[4] = 1;
                    segment[5] = 1;
                    segment[6] = 1;
                    break;
                case "9":
                    segment[0] = 1;
                    segment[1] = 1;
                    segment[2] = 1;
                    segment[3] = 1;
                    segment[4] = 0;
                    segment[5] = 1;
                    segment[6] = 1;
                    break;
                case "0":
                    segment[0] = 1;
                    segment[1] = 1;
                    segment[2] = 1;
                    segment[3] = 1;
                    segment[4] = 1;
                    segment[5] = 1;
                    segment[6] = 0;
                    break;
            }
        } else {

            switch (val) {
                case "null":
                    segment[0] = 0;
                    segment[1] = 0;
                    segment[2] = 0;
                    segment[3] = 0;
                    segment[4] = 0;
                    segment[5] = 0;
                    segment[6] = 0;
                    break;
                case "e":
                    segment[0] = 1;
                    segment[1] = 0;
                    segment[2] = 0;
                    segment[3] = 1;
                    segment[4] = 1;
                    segment[5] = 1;
                    segment[6] = 1;
                    break;
                case "r":
                    segment[0] = 0;
                    segment[1] = 0;
                    segment[2] = 0;
                    segment[3] = 0;
                    segment[4] = 1;
                    segment[5] = 0;
                    segment[6] = 1;
                    break;
                case "o":
                    segment[0] = 0;
                    segment[1] = 0;
                    segment[2] = 1;
                    segment[3] = 1;
                    segment[4] = 1;
                    segment[5] = 0;
                    segment[6] = 1;
                    break;
            }
        }
        repaint();
    }

    public synchronized void paint(Graphics g) {

        // górny
        Polygon p1 = new Polygon();
        p1.addPoint(20, 0);
        p1.addPoint(80, 0);
        p1.addPoint(90, 10);
        p1.addPoint(80, 20);
        p1.addPoint(20, 20);
        p1.addPoint(10, 10);

        if (segment[0] == 1) {
            g.setColor(activeColor);
            g.fillPolygon(p1);
        }
        if (segment[0] != 1) {
            g.setColor(inactiveColor);
            g.fillPolygon(p1);
        }

        // górny lewy
        Polygon p2 = new Polygon();
        p2.addPoint(10, 15);
        p2.addPoint(20, 25);
        p2.addPoint(20, 85);
        p2.addPoint(10, 95);
        p2.addPoint(0, 85);
        p2.addPoint(0, 25);

        if (segment[5] == 1) {
            g.setColor(activeColor);
            g.fillPolygon(p2);
        }
        if (segment[5] != 1) {
            g.setColor(inactiveColor);
            g.fillPolygon(p2);
        }

        // górny prawy
        Polygon p3 = new Polygon();
        p3.addPoint(90, 15);
        p3.addPoint(100, 25);
        p3.addPoint(100, 85);
        p3.addPoint(90, 95);
        p3.addPoint(80, 85);
        p3.addPoint(80, 25);

        if (segment[1] == 1) {
            g.setColor(activeColor);
            g.fillPolygon(p3);
        }
        if (segment[1] != 1) {
            g.setColor(inactiveColor);
            g.fillPolygon(p3);
        }

        // środkowy
        Polygon p4 = new Polygon();
        p4.addPoint(20, 90);
        p4.addPoint(80, 90);
        p4.addPoint(90, 100);
        p4.addPoint(80, 110);
        p4.addPoint(20, 110);
        p4.addPoint(10, 100);

        if (segment[6] == 1) {
            g.setColor(activeColor);
            g.fillPolygon(p4);
        }
        if (segment[6] != 1) {
            g.setColor(inactiveColor);
            g.fillPolygon(p4);
        }

        // dolny
        Polygon p5 = new Polygon();
        p5.addPoint(20, 180);
        p5.addPoint(80, 180);
        p5.addPoint(90, 190);
        p5.addPoint(80, 200);
        p5.addPoint(20, 200);
        p5.addPoint(10, 190);

        if (segment[3] == 1) {
            g.setColor(activeColor);
            g.fillPolygon(p5);
        }
        if (segment[3] != 1) {
            g.setColor(inactiveColor);
            g.fillPolygon(p5);
        }

        // dolny lewy
        Polygon p6 = new Polygon();
        p6.addPoint(10, 105);
        p6.addPoint(20, 115);
        p6.addPoint(20, 175);
        p6.addPoint(10, 185);
        p6.addPoint(0, 175);
        p6.addPoint(0, 115);

        if (segment[4] == 1) {
            g.setColor(activeColor);
            g.fillPolygon(p6);
        }
        if (segment[4] != 1) {
            g.setColor(inactiveColor);
            g.fillPolygon(p6);
        }

        // dolny prawy
        Polygon p7 = new Polygon();
        p7.addPoint(90, 105);
        p7.addPoint(100, 115);
        p7.addPoint(100, 175);
        p7.addPoint(90, 185);
        p7.addPoint(80, 175);
        p7.addPoint(80, 115);

        if (segment[2] == 1) {
            g.setColor(activeColor);
            g.fillPolygon(p7);
        }
        if (segment[2] != 1) {
            g.setColor(inactiveColor);
            g.fillPolygon(p7);
        }

    }

}
